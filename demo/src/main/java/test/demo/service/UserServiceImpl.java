package test.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import test.demo.entity.User;
import test.demo.model.dto.UserDto;
import test.demo.model.mapper.UserMapper;

@Component
public class UserServiceImpl implements UserService {
	private static ArrayList<User> users = new ArrayList<User>();
	static {
		users.add(new User(1, "Hoang", "hoang@gmail.com", "aaa", "aa", "asd"));
	}
	@Override
	public List<UserDto> getListUser() {
		List<UserDto> result = new ArrayList<UserDto>();
		for(User user : users) {
			result.add(UserMapper.toUserDto(user));
		}
		return result;
	}
	@Override
	public UserDto getUserById(int id) {
		for(User user : users) {
			if(user.getId() == id) {
				return UserMapper.toUserDto(user);
			}
		}
		return null;
	}
	@Override
	public List<UserDto> searchUser(String keyword) {
		List<UserDto> result = new ArrayList<UserDto>();
		for(User user : users) {
			if(user.getName().contains(keyword)) {
				 result.add(UserMapper.toUserDto(user));
			}
		}
		return result;
	}
}
