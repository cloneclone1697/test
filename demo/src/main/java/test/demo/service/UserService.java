package test.demo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import test.demo.entity.User;
import test.demo.model.dto.UserDto;

@Service
public interface UserService {
public List<UserDto> getListUser();
public UserDto getUserById(int id);
public List<UserDto> searchUser(String keyword);
}
