package test.demo.controller;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import test.demo.entity.User;
import test.demo.entity.User2;
import test.demo.model.dto.UserDto;
import test.demo.service.UserService;

@RestController
@RequestMapping("/users")
public class MyController {
	@Autowired
	private UserService userService;
	private static final Logger LOG = LoggerFactory.getLogger(MyController.class);

	@GetMapping("/search")
	public ResponseEntity<?> searchUser(
			@RequestParam(value = "keyword", required = false, defaultValue = "") String name) {
		List<UserDto> users = userService.searchUser(name);
		return ResponseEntity.ok(users);
	}

	@GetMapping("")
	public ResponseEntity<?> getListUser() {
		List<UserDto> users = userService.getListUser();
		return ResponseEntity.ok(users);
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getUserById(@RequestParam @PathVariable int id) {
		UserDto result = userService.getUserById(id);
		return ResponseEntity.ok(result);
	}

	@PostMapping("")
	public ResponseEntity<?> createUser() {
		return null;
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> updateUser() {
		return null;
	}

	@DeleteMapping("")
	public ResponseEntity<?> deleteUser() {
		return null;
	}
	
	@GetMapping("/test1")
	public String testRequestHeader (@RequestHeader String authorization) {
		System.out.println("printing the auth "+authorization);
		return "Success";
	}
	
	@GetMapping("/test2")
	public String handleRequestHeader (@RequestHeader Map<String, String> mapValues) {
		System.out.println("printing the header"+mapValues);
		return "Success";
	}
	@PostMapping("/test3")
	public void printData(@RequestBody User2 user2) {
		System.out.println("Printing the user data:"+user2);
	}
}
