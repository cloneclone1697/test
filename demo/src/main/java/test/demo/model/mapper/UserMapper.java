package test.demo.model.mapper;

import test.demo.entity.User;
import test.demo.model.dto.UserDto;

public class UserMapper {
	public static UserDto toUserDto(User user) {
		UserDto tmp = new UserDto();
		tmp.setId(user.getId());
		tmp.setEmail(user.getEmail());
		tmp.setAvatar(user.getAvatar());
		tmp.setName(user.getName());
		tmp.setPhone(user.getPhone());
		return tmp;
	}
}
